
$(document).ready(function() {
  var soruce = $('.carousel-item img').attr('src');

  var alto = $(window).height();
  var ancho = $(window).width();
  var directorio = 'responsive/';
  var directorioM = 'responsive/mobile/';
  var source = $('.slider .slides li img').attr('data-image');
  if(ancho <=600){
    $('.arrowresponsible').click(function() {
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
      $('.responsible').css('display','none');
    });
    $('.orange-btn').click(function(event) {
      $(this).removeClass('modal-trigger');
      $('.responsible').css('display','block');
      $('.responsible').css('transition','1s');
      $('.text-carrucel').css('display','none');
      $('.arrowresponsible').css('display','block');
    });
    $('#modal1').removeClass('modal');
    $('#modal1').removeClass('bottom-sheet');
    $('#modal1').addClass('responsible');
    $('.slider .slides li img').each(function() {
      var source = $(this).attr('data-image');
      $(this).attr('src', directorioM+source);
    });
  }else if(ancho <=995){
    $('.arrowresponsible').click(function() {
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
      $('.responsible').css('display','none');
    });
    $('.orange-btn').click(function(event) {
      $(this).removeClass('modal-trigger');
      $('.responsible').css('display','block');
      $('.responsible').css('transition','1s');
      $('.text-carrucel').css('display','none');
      $('.arrowresponsible').css('display','block');
    });
    $('#modal1').removeClass('modal');
    $('#modal1').removeClass('bottom-sheet');
    $('#modal1').addClass('responsible');
    $('.slider .slides li img').each(function() {
      var source = $(this).attr('data-image');
      $(this).attr('src', directorio+source);
    });

  }else{
    if($('#modal1').hasClass('modal')){

    }else{
      $('#modal1').addClass('modal');
      $('#modal1').addClass('bottom-sheet');
      $('.orange-btn').addClass('modal-trigger');
      $('#modal1').removeClass('responsible');
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
    };

    /*$('.slider .slides li img').each(function() {
        var source = $(this).attr('data-image');
      $(this).attr('src', source)
    });*/
  }

});
/****************************** CADA QUE CAMBIA EL TAMAÑO DE PANTALLA ***********************************************/
$(window).resize(function() {
  var anchonoticias =  $('.notocias .slides').width();
  $('.noticias .slides li').css('width', anchonoticias);

  var alto = $(window).height();
  var ancho = $(window).width();
  var directorio = 'responsive/';
  var directorioM = 'responsive/mobile/';
  if(ancho <=600){
    $('.arrowresponsible').click(function() {
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
      $('.responsible').css('display','none');
    });
    $('.orange-btn').click(function(event) {
      $(this).removeClass('modal-trigger');
      $('.responsible').css('display','block');
      $('.responsible').css('transition','1s')
      $('.text-carrucel').css('display','none');
      $('.arrowresponsible').css('display','block');
    });
    $('#modal1').removeClass('modal');
    $('#modal1').removeClass('bottom-sheet');
    $('#modal1').addClass('responsible');
    $('.slider .slides li img').each(function(){
      var source = $(this).attr('data-image');
      $(this).css('background-image', 'url('+directorioM.concat(source)+')');
    });
  }else if(ancho <= 995){
    $('.arrowresponsible').click(function() {
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
      $('.responsible').css('display','none');
    });
    $('.orange-btn').click(function(event) {
      $(this).removeClass('modal-trigger');
      $('.responsible').css('display','block');
      $('.responsible').css('transition','1s')
      $('.text-carrucel').css('display','none');
      $('.arrowresponsible').css('display','block');
    });
    $('#modal1').removeClass('modal');
    $('#modal1').removeClass('bottom-sheet');
    $('#modal1').addClass('responsible');

    $('.slider .slides li img').each(function(){
      var source = $(this).attr('data-image');
        $(this).css('background-image', 'url('+directorio.concat(source)+')');
    });
  }else{
    if($('#modal1').hasClass('modal')){

    }else{
      $('#modal1').addClass('modal');
      $('#modal1').addClass('bottom-sheet');
      $('.orange-btn').addClass('modal-trigger');
      $('#modal1').removeClass('responsible');
      $('.text-carrucel').css('display','block')
    }

    $('.slider .slides li img').each(function(){
      var source = $(this).attr('data-image');
      $(this).css('background-image', 'url('+source+')');
    });
  }
});
