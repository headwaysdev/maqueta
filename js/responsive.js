
$(document).ready(function() {
  var soruce = $('.carousel-item img').attr('src');

  var alto = $(window).height();
  var ancho = $(window).width();
  var directorio = 'responsive/';
  var directorioM = 'responsive/mobile/';
  var source = $('.slider .slides li img').attr('data-image');
  if(ancho <=600){
    $('.type_modal').removeClass('modal');
    $('.type_modal').removeClass('bottom-sheet');
    $('.type_modal').addClass('responsible');
    console.log('Mobile responsive 600');
    $('.arrowresponsible').click(function() {
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
      $('.responsible').css('display','none');
    });
    $('.orange-btn').click(function(event) {
      $(this).removeClass('modal-trigger');
      var modalreferido = $(this).attr('href');
      console.log(modalreferido);
      $(modalreferido).css('display','block');
      $(modalreferido).css('transition','1s');
      $('.text-carrucel').css('display','none');
      $('.arrowresponsible').css('display','block');
    }); 
    $('.slider .slides li img').each(function() {
      var source = $(this).attr('data-image');
      $(this).attr('src', directorioM+source);
    });
  }else if(ancho <=992){

    console.log('Límite responsive tablet 992');
    $('.type_modal').removeClass('modal');
    $('.type_modal').removeClass('bottom-sheet');
    $('.type_modal').addClass('responsible');
    $('.arrowresponsible').click(function() {
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
      $('.responsible').css('display','none');
    });
    $('.orange-btn').click(function(event) {
       $(this).removeClass('modal-trigger');
      var modalreferido = $(this).attr('href');
      $(modalreferido).css('display','block');
      $(modalreferido).css('transition','1s');
      $('.text-carrucel').css('display','none');
      $('.arrowresponsible').css('display','block');
    });
    $('.slider .slides li img').each(function() {
      var source = $(this).attr('data-image');
      $(this).attr('src', directorio+source);
    });

  }else{

      $('orange-btn').addClass('modal-trigger');
      $('.arrowresponsible').css('display','none');

      $('.orange-btn').click(function(event) {
        $(this).addClass('modal-trigger');
        $('.responsible').css('display','none');
        $('.text-carrucel').css('display','block');
        $('.arrowresponsible').css('display','none');
    });


    if($('.type_modal').hasClass('modal')){

    }else{
      $('.responsible').addClass('modal');
      $('.responsible').addClass('bottom-sheet');
      $('.orange-btn').addClass('modal-trigger');
      $('.responsible').removeClass('responsible');
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
    };

    /*$('.slider .slides li img').each(function() {
        var source = $(this).attr('data-image');
      $(this).attr('src', source)
    });*/
  }

});
/****************************** CADA QUE CAMBIA EL TAMAÑO DE PANTALLA ***********************************************/
$(window).resize(function() {
  var anchonoticias =  $('.notocias .slides').width();
  $('.noticias .slides li').css('width', anchonoticias);

  var alto = $(window).height();
  var ancho = $(window).width();
  var directorio = 'responsive/';
  var directorioM = 'responsive/mobile/';
  if(ancho <=600){
    $('.arrowresponsible').click(function() {
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
      $('.responsible').css('display','none');
    });
    $('.orange-btn').click(function(event) {
       $(this).removeClass('modal-trigger');
      var modalreferido = $(this).attr('href');
      $(modalreferido).css('display','block');
      $(modalreferido).css('transition','1s');
      $('.text-carrucel').css('display','none');
      $('.arrowresponsible').css('display','block');
      //$('.type_modal').css('opacity','1');
      //$('.type_modal').css('transition','1s')
    });
    $('.type_modal').removeClass('modal');
    $('.type_modal').removeClass('bottom-sheet');
    $('.type_modal').addClass('responsible');
    $('.slider .slides li img').each(function(){
      var source = $(this).attr('data-image');
      $(this).css('background-image', 'url('+directorioM.concat(source)+')');
    });
  }else if(ancho <= 992){
    $('.arrowresponsible').click(function() {
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
      $('.responsible').css('display','none');
    });
    $('.orange-btn').click(function(event) {
       $(this).removeClass('modal-trigger');
      var modalreferido = $(this).attr('href');
      $(modalreferido).css('display','block');
      $(modalreferido).css('transition','1s');
      $('.text-carrucel').css('display','none');
      $('.arrowresponsible').css('display','block');
      $('.type_modal').css('opacity','1');
      $('.type_modal').css('transition','1s')
    });
    $('.type_modal').removeClass('modal');
    $('.type_modal').removeClass('bottom-sheet');
    $('.type_modal').addClass('responsible');

    $('.slider .slides li img').each(function(){
      var source = $(this).attr('data-image');
        $(this).css('background-image', 'url('+directorio.concat(source)+')');
    });
  }else{
    $('orange-btn').addClass('modal-trigger');
    $('.arrowresponsible').css('display','none');

    $('.orange-btn').click(function(event) {
      $(this).addClass('modal-trigger');
      $('.responsible').css('display','none');
      $('.text-carrucel').css('display','block');
      $('.arrowresponsible').css('display','none');
    });

    if($('.type_modal').hasClass('modal')){

    }else{
      $('.type_modal').addClass('modal');
      $('.type_modal').addClass('bottom-sheet');
      $('.orange-btn').addClass('modal-trigger');
      $('.type_modal').removeClass('responsible');
      $('.text-carrucel').css('display','block')
    }

    $('.slider .slides li img').each(function(){
      var source = $(this).attr('data-image');
      $(this).css('background-image', 'url('+source+')');
    });
  }
});
