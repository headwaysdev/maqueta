
[Headways.io](https://headways.io) 

![Scheme](http://gnp.com.mx/informes/informe-anual-2015/gnp.2015.33reports.com/img/logo.jpg)   

## GNP Mobile

Mobile Layout (Responsive)
Support: [@EduardoTufino](http://eduardo.xyz) 

## Table of Contents
- [Supported Browsers](#supported-browsers)
- [Changelog](#changelog)
- [Testing](#testing)
- [Copyright and license](#copyright-and-license)


## Supported Browsers:
GNP Mobile is compatible with:

- Chrome 35+
- Firefox 31+
- Safari 7+
- Opera
- Edge
- IE 10+

## Changelog
For changelogs, check out [the Releases section of materialize](https://github.com/Dogfalo/materialize/releases) or the [CHANGELOG.md](CHANGELOG.md).

## Testing
We use Jasmine as our testing framework and we're trying to write a robust test suite for our components. If you want to help, [here's a starting guide on how to write tests in Jasmine](CONTRIBUTING.md#jasmine-testing-guide).


## Copyright and license
Code copyright 2017 Headways.io Code released under the MIT license.
